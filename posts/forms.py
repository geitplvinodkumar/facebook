from django import forms
from django.contrib.auth.models import User
from posts.models import Post

class PostForm(forms.ModelForm):
  class Meta:
    model = Post
    fields = ['write_post', 'pic']
    widgets = {
            'write_post': forms.Textarea(
                attrs={'id': 'post-title', 'required': True, 'placeholder': 'Say something...'}
            ),
        }

class EditForm(forms.ModelForm):
  class Meta:
    model = Post
    fields = ['pic']