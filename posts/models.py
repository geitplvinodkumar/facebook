from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify

# def generate_filename(self, filename):
#     url = "documents/%s/%s" % (self.user.username, filename)
#     return url

# Create your models here.
class Post(models.Model):
  write_post = models.TextField(blank=True)
  slug = models.SlugField(unique=True, null=True)
  pic = models.FileField("Image", upload_to='static/documents')  
  name = models.CharField(max_length=100)
  user = models.ForeignKey(User, null=True,blank=True)
  upload_date = models.DateTimeField(auto_now_add=True)
  updated_date = models.DateTimeField(default=timezone.now)

  def __unicode__(self):
      return self.user

  @models.permalink
  def get_absolute_url(self):
    return ('edit_post', (), 
            {
              'slug' :self.slug,
            })

  def save(self, *args, **kwargs):
    if not self.slug:
     self.slug = slugify(self.write_post)
    super(Post, self).save(*args, **kwargs)
