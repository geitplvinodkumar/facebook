import re
from django import forms
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from django.utils.safestring import mark_safe
from profiles.models import Profile

class RegistrationForm(forms.Form):
 
    username  = forms.RegexField(regex=r'^\w+$', widget=forms.TextInput(attrs={'required': True, 'placeholder': 'Username', 'max_length': 30 }), error_messages={ 'invalid': _("This value must contain only letters, numbers and underscores.") })
    firstname = forms.RegexField(regex=r'^\w+$', widget=forms.TextInput(attrs={'required': True, 'placeholder': 'Firstname', 'max_length': 30 }), error_messages={ 'invalid': _("This value must contain only letters, numbers and underscores.") })
    lastname  = forms.RegexField(regex=r'^\w+$', widget=forms.TextInput(attrs={'required': True, 'placeholder': 'Lastname', 'max_length': 30 }), error_messages={ 'invalid': _("This value must contain only letters, numbers and underscores.") })
    password  = forms.CharField(widget=forms.PasswordInput(attrs={'required': True, 'placeholder': 'Password', 'max_length': 30, 'render_value': False } ))
    password_ = forms.CharField(widget=forms.PasswordInput(attrs={'required': True, 'placeholder': 'Password again', 'max_length': 30, 'render_value': False  }))
 
    def clean_username(self):
        try:
            user = User.objects.get(username__iexact=self.cleaned_data['username'])
        except User.DoesNotExist:
            return self.cleaned_data['username']
        raise forms.ValidationError(_("The username already exists. Please try another one."))
 
    def clean(self):
        if 'password' in self.cleaned_data and 'password' in self.cleaned_data:
            if self.cleaned_data['password'] != self.cleaned_data['password_']:
                raise forms.ValidationError(_("The two password fields did not match."))
        return self.cleaned_data

class HorizontalRadioRenderer(forms.RadioSelect.renderer):
  def render(self):
    return mark_safe(u'\n'.join([u'%s\n' % w for w in self]))

CHOICES=[('male','Male'),('female','Female')]
class RegistrationProfileForm(forms.ModelForm):
   class Meta:
    model = Profile
    fields = ['gender', 'date_of_birth']
    widgets = {
            'gender': forms.RadioSelect(
                renderer=HorizontalRadioRenderer , attrs={'required': True,} , choices=CHOICES 
            ),
            'date_of_birth': forms.TextInput(
                attrs={'class':'datepicker', 'required': True,}
            ),
        }