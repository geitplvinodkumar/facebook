from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify
# Create your models here.
class Like(models.Model):
  username = models.CharField(max_length=30)
  likes = models.BooleanField(default=False)
  product_name = models.CharField(max_length=30)
  product_id = models.IntegerField(null=True,blank=True)
  user = models.ForeignKey(User, null=True,blank=True, related_name='user_id')
  updated_date = models.DateTimeField(default=timezone.now)

  @property    
  def is_active(self):
    return bool(self.likes)