from django.shortcuts import render
from django.http import HttpResponse
try:
    from django.utils import simplejson as json
except ImportError:
    import json
from django.shortcuts import get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_protect

from likes.models import Like
# Create your views here.
@csrf_protect
@login_required(login_url='/login/home/')
def like(request):
  user = request.user
  post_id = None
  if request.method == 'POST' or 'GET':
    product_id = request.POST['post_id']
    like = Like.objects.get(id=product_id)
    if like.product_id != product_id and like.user_id != user.id:
      Like.objects.create(
        username = user.username,
        user = user,
        product_id = product_id,
        )
    else :
      if like.product_id == product_id and like.user_id == user.id:
        likes = like.likes - 1
        like.likes = likes
        like.save()
      else:
        likes = like.likes + 1
        like.likes = likes
        like.save()
    return HttpResponse(likes)