
  $(document).ready(function(){
    $('.panel_post').hide();
    $(".comment_button").click(function(){

      var element = $(this);
      var I = element.attr("id");

      $("#slidepanel"+I).slideDown(300);
      $(this).toggleClass("active"); 

      return false;
    });
  });
 
  $(document).on('submit','.comment_form_id',function(e) { 
    e.preventDefault();
    var element = $(this);
    var post_id = element.attr("id");
    $.ajax({ 
      type: 'POST',
      url:'/create/comment/',
      data:{
        post_id,
        comment_text : $('#text_id').val(),
        'csrfmiddlewaretoken' : $('input[name=csrfmiddlewaretoken]').val()
      },
      // dataType: "json",
      success: function() { // on success..
        $('#text_id').val(""); 
      },
      error: function() { // on error..
        alert('comment unsuccessfull')
      }
    });
    return false;
  });
