import json
from django.core import serializers
from django.shortcuts import render, redirect, render_to_response
from comments.forms import *
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_protect
from django.template import RequestContext, Context, loader 
from django.http import *
from django.template import Context
from django.utils import timezone
from datetime import datetime, timedelta
@csrf_protect
@login_required(login_url='/login/home/')
def create_comment(request):
  user = request.user
  author = user.username
  # post_id = id
  if request.method == "POST":
    # import pdb; pdb.set_trace()
    text = request.POST['comment_text']
    post_key = request.POST['post_id']
    Comment.objects.create(
      text = text,
      user = user,
      author = author,
      post_id = post_key,
      )
    return redirect('/home')
  return HttpResponse('')


@login_required(login_url='/login/home/')
def show_comment(request):
  return render_to_response('login/home.html',{'comment_list': Comment.objects.all() }, 
    context_instance=RequestContext(request))